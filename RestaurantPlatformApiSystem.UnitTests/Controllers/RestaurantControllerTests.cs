﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xunit;
using Moq;
using RestaurantPlatformApiSystem.WebApi.Controllers;
using RestaurantPlatformApiSystem.Domain.Entities;
using RestaurantPlatformApiSystem.Domain.Abstract;


namespace RestaurantPlatformApiSystem.UnitTests.Controllers
{
    public class RestaurantControllerTests
    {
        private Restaurant[] restaurantTestData = new Restaurant[]
        {
             new Restaurant { RestaurantID = 1, RestaturantName = "Mezzaluna", Adresse = "Vangede Bygade 88, Gentofte,2820", Postale = "2820", OpenStart = new TimeSpan(14, 30, 0), OpenEnd = new TimeSpan(22, 30, 0), RestaurantLogo = "18283.gif" },
             new Restaurant { RestaurantID = 2, RestaturantName = "Pizzamed", Adresse = "Vangedevej 2, Gentofte,2820", Postale = "2820",           OpenStart = new TimeSpan(14, 30, 0),  OpenEnd= new TimeSpan(22, 30, 0), RestaurantLogo = "15120.gif" },
             new Restaurant { RestaurantID = 3, RestaturantName = "Big Bens Pizza & Pasta", Adresse = "Baunegårdsvej 7, Gentofte, 2820", Postale = "2820", OpenStart = new TimeSpan(14, 30, 0), OpenEnd = new TimeSpan(21, 30, 0), RestaurantLogo = "11729.gif"},
             new Restaurant { RestaurantID = 4, RestaturantName = "NamNam Slik - Lyngby", Adresse = "Lyngby Torv 7, Kongens Lyngby, 2800", Postale = "2820", OpenStart = new TimeSpan(14, 30, 0), OpenEnd = new TimeSpan(22, 30, 0), RestaurantLogo = "18295.gif" }
        };

        [Fact]
        public void GetRestaurants()
        {
            Mock<IRestaurantRepository> mock = new Mock<IRestaurantRepository>();
            mock.Setup(u => u.Restaurants).Returns(restaurantTestData);

            RestaurantController target = new RestaurantController(mock.Object);
            Restaurant[] result = ((IEnumerable<Restaurant>)target.Get()).ToArray();

            Assert.Equal(4, result.Length);
            Assert.Equal("Pizzamed", result[1].RestaturantName);
        }
    }
}
