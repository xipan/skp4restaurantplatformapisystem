﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Xunit;
using Moq;
using RestaurantPlatformApiSystem.WebApi.Controllers;
using RestaurantPlatformApiSystem.Domain.Entities;
using RestaurantPlatformApiSystem.Domain.Abstract;



namespace RestaurantPlatformApiSystem.UnitTests.Controllers
{

    public class UserControllerTests
    {
        private User[] userTestData = new User[] {
            new User { UserID = 1, FirstName = "Xi", AfterName = "Pan", Email = "xipanpost@gmail.com", PassWord = "password", UserType = 0 },
            new User { UserID = 2, FirstName = "Frederik", AfterName = "Jacobsen", Email = "frederik@gmail.com", PassWord = "password", UserType = 0 },
            new User { UserID = 3, FirstName = "Aksel", AfterName = "Nielsen", Email = "aksel@gmail.com", PassWord = "password", UserType = 0 },
            new User { UserID = 4, FirstName = "Julie", AfterName = "Berman ", Email = "julie@gmail.com", PassWord = "password", UserType = 0 }
        };
        [Fact]
        public void GetUsers()
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(u => u.Users).Returns(userTestData);

            UserController target = new UserController(mock.Object);
            User[] result = ((IEnumerable<User>)target.Get()).ToArray();

            Assert.Equal(4, result.Length);
            Assert.Equal("Frederik", result[1].FirstName);            

        }

        [Fact]
        public void GetUserByUserId()
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            mock.Setup(u => u.Users).Returns(userTestData);

            UserController target = new UserController(mock.Object);
            User result = ((User)target.Get(1));

            Assert.Equal("Xi", result.FirstName);
        }

        [Fact]
        public void AddUser()
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            UserController target = new UserController(mock.Object);
            User user = new User { UserID = 0 };

            Boolean result = target.Post(user);
            mock.Verify(m => m.SaveUser(user));
            mock.Verify(m => m.SaveUser(user), Times.Once);            
        }

        [Fact]
        public void EditUser()
        {
            Mock<IUserRepository> mock = new Mock<IUserRepository>();
            UserController target = new UserController(mock.Object);
            User user = new User { UserID = 1, FirstName = "Xi", AfterName = "Pan", Email = "xipanpost@gmail.com", PassWord = "password", UserType = 0 };

            Boolean result = target.Put(user);
            mock.Verify(m => m.SaveUser(user));
            mock.Verify(m => m.SaveUser(user), Times.Once);            
        }



    }
}
