﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private IRestaurantRepository repository;

        public RestaurantController(IRestaurantRepository restaurantRepository)
        {
            this.repository = restaurantRepository;
        }

        // GET: api/Restaurant
        [HttpGet]
        public IEnumerable<Restaurant> Get()
        {
            return this.repository.Restaurants.ToList();
        }

    }
}