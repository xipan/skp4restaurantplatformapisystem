﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private ICartRepository repository;

        public CartController(ICartRepository cartRepository)
        {
            this.repository = cartRepository;
        }

        // GET: api/Cart/GetCart
        [HttpGet]
        [Route("GetCart/{userId}/{restaurantId}")]
        
        public Cart Get(int userId, int restaurantId)
        {
            //return "The [Route] with multiple params worked:" + userId.ToString() + "  " + restaurantId.ToString(); 

            if (userId > 0)
            {
                Cart cart = this.repository.Carts.FirstOrDefault(u => u.UserID == userId && u.RestaurantID == restaurantId);
                if (cart != null)
                {
                    return cart;
                }
            }
            return null;
        }

        // POST: api/Cart
        [HttpPost]
        public Cart Post([FromBody] Cart value)
        {
            return repository.SaveCart(value);
        }
    }
}