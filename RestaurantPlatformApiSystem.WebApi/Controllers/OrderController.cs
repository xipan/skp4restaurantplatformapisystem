﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;
using RestaurantPlatformApiSystem.WebApi.ViewModel;


namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private IOrderRepository repository;

        public OrderController(IOrderRepository orderRepository)
        {
            this.repository = orderRepository;
        }

        // GET: api/Order/5
        [HttpGet("{userId}")]
        public List<OrderListByUser> Get(int userId)
        {
            if (userId > 0)
            {
                List<OrderListByUser> orders = this.repository.Orders
                    .Where(o => o.UserID == userId)
                    .OrderByDescending(o => o.CreateDT)
                    .Select( o => new OrderListByUser { 
                        OrderID = o.OrderID, 
                        UserID = o.UserID, 
                        Status = o.Status, 
                        CreateDT = o.CreateDT, 
                        RestaurantID = o.RestaurantID, 
                        RestaurantName = o.Restaurant.RestaturantName, 
                        OrderItem = o.OrderItem
                        .Select(oi => new OrderItem { 
                            OrderItemID = oi.OrderItemID, 
                            MenuItemName = oi.MenuItemName, 
                            Price = oi.Price, 
                            Quantity = oi.Quantity, 
                            Status = oi.Status, 
                            RefMenuItemID = oi.RefMenuItemID, 
                            OrderID = oi.OrderID })
                        .ToList()
                    })
                    .ToList();

                if (orders != null)
                {
                    return orders;
                }
            }
            return null;
        }

        // POST: api/Order/17
        [HttpPost]
        [Route("{cartId}")]
        public int Post(int cartId)
        {
            return repository.SaveOrder(cartId);
        }
    }
}