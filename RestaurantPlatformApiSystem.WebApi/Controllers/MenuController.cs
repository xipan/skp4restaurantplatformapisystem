﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private IMenuRepository repository;

        public MenuController(IMenuRepository menuRepository)
        {
            this.repository = menuRepository;
        }

        // GET: api/Menu/5
        [HttpGet("{restaurantId}")]
        public IEnumerable<MenuCategory> Get(int restaurantId)
        {
            if (restaurantId > 0)
            {
                //User user = this.repository..FirstOrDefault(u => u.UserID == userId);
                List<MenuCategory> menus = this.repository.Menus.Where(r => r.RestaurantID == restaurantId).ToList();

                if (menus != null)
                {
                    return menus;
                }

            }
            return null;
        }

    }
}