﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private IEmployeeRepository repository;

        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            this.repository = employeeRepository;
        }

        // GET: api/Employee
        [HttpGet]
        public IEnumerable<Employee> Get()
        {
            return this.repository.Employees.ToList();
        }

        // POST: api/Employee/Login
        [Route("Login")]
        [HttpPost]
        public User Login([FromBody] Employee value)
        {
            Employee employee = this.repository.Employees.FirstOrDefault(u => u.Email == value.Email && u.PassWord == value.PassWord);
            if (employee != null)
            {
                return employee;
            }

            return null;
        }
    }
}