﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private IUserRepository repository;

        public UserController(IUserRepository userRepository)
        {
            this.repository = userRepository;
        }

        // GET: api/User
        [HttpGet]
        public IEnumerable<User> Get()
        {
            return this.repository.Users.ToList();
        }


        // GET: api/User/5
        [HttpGet("{userId}")]
        public User Get(int userId)
        {
            if (userId > 0)
            {
                User user = this.repository.Users.FirstOrDefault(u => u.UserID == userId);
                if(user != null)
                {
                    return user;
                }
            }
            return null;
        }

        // POST: api/User
        [HttpPost]
        public bool Post([FromBody] User value)
        {
            if (repository.SaveUser(value) == 1) 
            { return true; }
            else
            { return false; }            
        }

        // POST: api/User/Login
        [Route("Login")]
        [HttpPost]
        public User Login([FromBody] User value)
        {
            User user = this.repository.Users.FirstOrDefault(u => u.Email == value.Email && u.PassWord == value.PassWord);
            if (user != null)
            {
                return user;
            }

            return null;
        }

        // PUT: api/User/5
        [HttpPut]
        public bool Put([FromBody] User value)
        {
            if (value.UserID > 0)
            {
                if (repository.SaveUser(value) == 1)
                { return true; }
            }
            return false;
        }

        // DELETE: api/User/5
        [HttpDelete("{userId}")]
        public bool Delete(int userId)
        {
            if (repository.DelUser(userId) == 1)
            { return true; }
            else
            { return false; }
        }

    }
}
