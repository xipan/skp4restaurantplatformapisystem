﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.WebApi.ViewModel
{
    public class OrderListByUser
    {
        public int OrderID { get; set; }
        public int UserID { get; set; }
        /// <summary>
        /// 0: Pending, 1: Complete
        /// </summary>
        public int Status { get; set; }
        public DateTime CreateDT { get; set; }

        public int RestaurantID { get; set; }
        public string RestaurantName { get; set; }

        public ICollection<OrderItem> OrderItem { get; set; }
    }
}
