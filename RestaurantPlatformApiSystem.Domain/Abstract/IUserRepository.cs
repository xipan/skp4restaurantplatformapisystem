﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;


namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface IUserRepository
    {
        IEnumerable<User> Users { get; }
        int SaveUser(User user);
        int DelUser(int userId);

    }
}
