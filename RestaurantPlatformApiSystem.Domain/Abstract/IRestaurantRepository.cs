﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface IRestaurantRepository
    {
        IEnumerable<Restaurant> Restaurants { get; }
        void SaveRestaurant(Restaurant restaurant);
    }
}
