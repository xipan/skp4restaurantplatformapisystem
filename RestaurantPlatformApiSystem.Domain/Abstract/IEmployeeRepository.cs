﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;


namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface IEmployeeRepository
    {
        IEnumerable<Employee> Employees { get; }
        int SaveEmployee(Employee employee);
        int DelEmployee(int employeeID);

    }
}
