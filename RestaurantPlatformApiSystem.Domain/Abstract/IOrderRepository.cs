﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;


namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface IOrderRepository
    {
        IEnumerable<Order> Orders { get; }
        int SaveOrder(int cartID);
        int DelOrder(int OrderID);
    }
}
