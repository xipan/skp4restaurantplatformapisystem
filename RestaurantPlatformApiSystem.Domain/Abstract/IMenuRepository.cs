﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;


namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface IMenuRepository
    {
        IEnumerable<MenuCategory> Menus { get; }

        void SaveMenu(MenuCategory menucategory);


    }
}
