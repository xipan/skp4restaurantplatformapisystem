﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Entities;


namespace RestaurantPlatformApiSystem.Domain.Abstract
{
    public interface ICartRepository
    {
        IEnumerable<Cart> Carts { get; }
        Cart SaveCart(Cart cart);
        int DelCart(int cartid);
    }
}
