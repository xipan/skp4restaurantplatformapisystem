﻿using System;
using System.Collections.Generic;
using System.Text;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFEmployeeRepository : IEmployeeRepository
    {
        private EFDbContext _context;

        public EFEmployeeRepository(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Employee> Employees 
        { 
            get
            {
                return _context.Employees;

            }
        }
        public int SaveEmployee(Employee employee)
        {
            return 0;
        }

        public int DelEmployee(int employeeID)
        {
            return 0;
        }

    }
}
