﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFMenuRepository : IMenuRepository
    {
        private EFDbContext _context;

        public EFMenuRepository(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<MenuCategory> Menus
        {
            get
            {
                return _context.MenuCategories.Include(mi => mi.MenuItem);
                //.Include(r => r.Table)
            }
        }

        public void SaveMenu(MenuCategory menucategory)
        {            

        }

    }
}
