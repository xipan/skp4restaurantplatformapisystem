﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public EFDbContext(DbContextOptions<EFDbContext> options)
    : base(options)
        { }

        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Table> Tables { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }

        public DbSet<MenuCategory> MenuCategories { get; set; }
        public DbSet<MenuItem> MenuItems { get; set; }

        public DbSet <Cart>Carts { get; set; }
        public DbSet <CartItem> CartItems { get; set; }

        public DbSet <Order>Orders { get; set; }
        public DbSet <OrderItem>OrderItems { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            //one to many Restaurant -> Table
            modelBuilder.Entity<Restaurant>()
                .HasMany(t => t.Table)
                .WithOne(r => r.Restaurant)
                .HasForeignKey(t => t.RestaurantID);

            //one to many Restaurant -> Order
            modelBuilder.Entity<Restaurant>()
                .HasMany(r => r.Order)
                .WithOne(o => o.Restaurant)
                .HasForeignKey(o => o.RestaurantID);

            //one to many Restaurant -> MenuCategory
            modelBuilder.Entity<Restaurant>()
                .HasMany(t => t.MenuCategory)
                .WithOne(m => m.Restaurant)
                .HasForeignKey(m => m.RestaurantID);

            //one to many MenuCategory -> MenuItem
            modelBuilder.Entity<MenuCategory>()
                .HasMany(m => m.MenuItem)
                .WithOne(m => m.MenuCategory)
                .HasForeignKey(m => m.MenuCategoryID);

            //one to many Cart -> CartItem
            modelBuilder.Entity<Cart>()
                .HasMany(c => c.CartItem)
                .WithOne(ci => ci.Cart)
                .HasForeignKey(ci => ci.CartID);

            //one to many Order -> OrderIttem
            modelBuilder.Entity<Order>()
                .HasMany(o => o.OrderItem)
                .WithOne(oi => oi.Order)
                .HasForeignKey(oi => oi.OrderID);


            modelBuilder.Entity<User>().HasData(
                new User { UserID = 1, FirstName = "Xi", AfterName = "Pan", Email = "xipanpost@gmail.com", PassWord = "password", UserType = 0 },
                new User { UserID = 2, FirstName = "Frederik", AfterName = "Jacobsen", Email = "frederik@gmail.com", PassWord = "password", UserType = 0 },
                new User { UserID = 3, FirstName = "Aksel", AfterName = "Nielsen", Email = "aksel@gmail.com", PassWord = "password", UserType = 0 },
                new User { UserID = 4, FirstName = "Julie", AfterName = "Berman ", Email = "julie@gmail.com", PassWord = "password", UserType = 0 }
            );

            modelBuilder.Entity<Restaurant>().HasData(
                new Restaurant()
                {
                    RestaurantID = 1,
                    RestaturantName = "Mezzaluna",
                    Adresse = "Vangede Bygade 88, Gentofte,2820",
                    Postale = "2820",
                    OpenEnd = new TimeSpan(14, 30, 0),
                    OpenStart = new TimeSpan(22, 30, 0),
                    RestaurantLogo = "18283.gif"
                },
                new Restaurant()
                {
                    RestaurantID = 2,
                    RestaturantName = "Pizzamed",
                    Adresse = "Vangedevej 2, Gentofte,2820",
                    Postale = "2820",
                    OpenEnd = new TimeSpan(14, 30, 0),
                    OpenStart = new TimeSpan(22, 30, 0),
                    RestaurantLogo = "15120.gif"
                },
                new Restaurant()
                {
                    RestaurantID = 3,
                    RestaturantName = "Big Bens Pizza & Pasta",
                    Adresse = "Baunegårdsvej 7, Gentofte, 2820",
                    Postale = "2820",
                    OpenEnd = new TimeSpan(14, 30, 0),
                    OpenStart = new TimeSpan(21, 30, 0),
                    RestaurantLogo = "11729.gif"
                },
                new Restaurant()
                {
                    RestaurantID = 4,
                    RestaturantName = "NamNam Slik - Lyngby",
                    Adresse = "Lyngby Torv 7, Kongens Lyngby, 2800",
                    Postale = "2820",
                    OpenEnd = new TimeSpan(14, 30, 0),
                    OpenStart = new TimeSpan(22, 30, 0),
                    RestaurantLogo = "18295.gif"
                }
            );


            modelBuilder.Entity<Table>().HasData(
                new Table() { TableID = 1, TableLabel = "1", TableStatus = 0, RestaurantID = 1 },
                new Table() { TableID = 2, TableLabel = "2", TableStatus = 0, RestaurantID = 1 },
                new Table() { TableID = 3, TableLabel = "3", TableStatus = 0, RestaurantID = 1 },
                new Table() { TableID = 4, TableLabel = "1", TableStatus = 0, RestaurantID = 2 },
                new Table() { TableID = 5, TableLabel = "2", TableStatus = 0, RestaurantID = 2 },
                new Table() { TableID = 6, TableLabel = "3", TableStatus = 0, RestaurantID = 2 },
                new Table() { TableID = 7, TableLabel = "1", TableStatus = 0, RestaurantID = 3 },
                new Table() { TableID = 8, TableLabel = "2", TableStatus = 0, RestaurantID = 3 },
                new Table() { TableID = 9, TableLabel = "3", TableStatus = 0, RestaurantID = 3 },
                new Table() { TableID = 10, TableLabel = "1", TableStatus = 0, RestaurantID = 4 },
                new Table() { TableID = 11, TableLabel = "2", TableStatus = 0, RestaurantID = 4 },
                new Table() { TableID = 12, TableLabel = "3", TableStatus = 0, RestaurantID = 4 },
                new Table() { TableID = 13, TableLabel = "4", TableStatus = 0, RestaurantID = 4 }
            );

            modelBuilder.Entity<MenuCategory>().HasData(
                new MenuCategory { MenuCategoryID = 1, CategoryName = "Drikke", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 2, CategoryName = "Menuer", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 3, CategoryName = "Salater", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 4, CategoryName = "Husets sandwich", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 5, CategoryName = "Pizzasandwich", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 6, CategoryName = "Pizza'er", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 7, CategoryName = "Børne Pizza", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 8, CategoryName = "Pastaretter", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 9, CategoryName = "A la carte", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 10, CategoryName = "Hjemmelavet Burger", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 11, CategoryName = "Hjemmelavet Pitabrød", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 12, CategoryName = "Diverse", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 13, CategoryName = "Bagt kartoffel", RestaurantID = 1 },
                new MenuCategory { MenuCategoryID = 14, CategoryName = "Drikkevarer", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 15, CategoryName = "Amerikansk & special slik", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 16, CategoryName = "Chokolade", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 17, CategoryName = "Chokoladeplader", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 18, CategoryName = "Amerikanske og specielle drikkevarer", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 19, CategoryName = "Nødder", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 20, CategoryName = "Sjov slik", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 21, CategoryName = "Slikposer", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 22, CategoryName = "Snacks", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 23, CategoryName = "Gum & Pastilles", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 24, CategoryName = "Is", RestaurantID = 4 },
                new MenuCategory { MenuCategoryID = 25, CategoryName = "Mix Menu", RestaurantID = 4 }
            );


            modelBuilder.Entity<MenuItem>().HasData(
                new MenuItem { MenuItemID = 1, MenuItemName = "Coca-Cola(0.5L)", Price = 20.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 2, MenuItemName = "Coca-Cola Zero(0.5L)", Price = 20.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 3, MenuItemName = "Tuborg(Dåse)", Price = 12.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 4, MenuItemName = "Faxe Kondi(0.5L)", Price = 12.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 5, MenuItemName = "Kildevand(0.5L)", Price = 10.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 6, MenuItemName = "Cocio", Price = 20.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 7, MenuItemName = "Nestea", Price = 20.00f, MenuCategoryID = 1 },
                new MenuItem { MenuItemID = 8, MenuItemName = "Husets Hvidvin", Price = 69.00f, MenuCategoryID = 1 },

                new MenuItem { MenuItemID = 9, MenuItemName = "Menu 1", Price = 69.00f, Description = "Hjemmelavet big burger, mellem pommes frites, salatmayonnaise og dåse sodavand", MenuCategoryID = 2 },
                new MenuItem { MenuItemID = 10, MenuItemName = "Menu 2", Price = 69.00f, Description = "Hjemmelavet pitabrød, mellem pommes frites, salatmayonnaise og dåse sodavand", MenuCategoryID = 2 },
                new MenuItem { MenuItemID = 11, MenuItemName = "Menu 3", Price = 79.00f, Description = "Hjemmelavet husets sandwich, mellem pommes frites, salatmayonnaise og dåse sodavand", MenuCategoryID = 2 },
                new MenuItem { MenuItemID = 12, MenuItemName = "Menu 4", Price = 89.00f, Description = "Hjemmelavet durum rulle, mellem pommes frites, salatmayonnaise og 0,5L sodavand", MenuCategoryID = 2 },
                new MenuItem { MenuItemID = 13, MenuItemName = "Menu 5", Price = 89.00f, Description = "Hjemmelavet pizza sandwich, mellem pommes frites, salatmayonnaise og 0,5L sodavand", MenuCategoryID = 2 },

                new MenuItem { MenuItemID = 14, MenuItemName = "Lille Bakke", Price = 25.00f, Description = "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", MenuCategoryID = 3 },
                new MenuItem { MenuItemID = 15, MenuItemName = "Mellem Bakke", Price = 40.00f, Description = "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", MenuCategoryID = 3 },
                new MenuItem { MenuItemID = 16, MenuItemName = "Lille Bakke", Price = 25.00f, Description = "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", MenuCategoryID = 3 },
                new MenuItem { MenuItemID = 17, MenuItemName = "Kyllingespyd (2 Stk.) med 3 Slags Salater", Price = 80.00f, MenuCategoryID = 3 },

                new MenuItem { MenuItemID = 18, MenuItemName = "Aqua Vand 0,50 L(Med brus)", Price = 14.00f, MenuCategoryID = 14 },
                new MenuItem { MenuItemID = 19, MenuItemName = "Capri-Sonne 330 ml.(Med brus)", Price = 14.00f, MenuCategoryID = 14 },
                new MenuItem { MenuItemID = 20, MenuItemName = "Capri-Sonne 330 ml.(Orange & Peach)", Price = 14.00f, MenuCategoryID = 14 },                
                new MenuItem { MenuItemID = 21, MenuItemName = "Gazoz 0, 33 L", Price = 12.00f, MenuCategoryID = 14 },
                new MenuItem { MenuItemID = 22, MenuItemName = "Fanta 0,33 L", Price = 12.00f, MenuCategoryID = 14 },
                new MenuItem { MenuItemID = 23, MenuItemName = "Fanta Exotic 0,33 L", Price = 12.00f, MenuCategoryID = 14 },

                new MenuItem { MenuItemID = 24, MenuItemName = "M&M's(Peanut Butter 46g)", Price = 20.00f, MenuCategoryID = 15 },
                new MenuItem { MenuItemID = 25, MenuItemName = "M&M's(White Chocolate 46g)", Price = 20.00f, MenuCategoryID = 15 },
                new MenuItem { MenuItemID = 26, MenuItemName = "M&M's(Peanut Butter 85g)", Price = 34.00f, MenuCategoryID = 15 }

            );


        }

}
}
