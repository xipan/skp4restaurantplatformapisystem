﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFOrderRepository : IOrderRepository
    {
        private EFDbContext _context;

        public EFOrderRepository(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Order> Orders
        {
            get
            {
                return _context.Orders
                    .Include(o => o.OrderItem)
                    .Include(o => o.Restaurant);                    
            }
        }

        public int SaveOrder(int cartID)
        {
            if (cartID > 0)
            {
                Cart currentCart = _context.Carts.Include(c => c.CartItem).Where(c => c.CartID == cartID).First();
                if (currentCart.CartItem != null)
                {
                    //Create OrderItem
                    List<OrderItem> newOrderItems = new List<OrderItem>();
                    foreach(CartItem item in currentCart.CartItem)
                    {        
                        newOrderItems.Add(new OrderItem { MenuItemName = item.MenuItemName , Price = item.Price, Quantity = item.Quantity, Status = 0, RefMenuItemID = item.RefMenuItemID });
                    }

                    Order newOrder = new Order { UserID = currentCart.UserID, Status = 0, CreateDT = DateTime.Now, RestaurantID = currentCart.RestaurantID, OrderItem = newOrderItems };
                    _context.Orders.Add(newOrder);

                    //Delete Cart
                    _context.Carts.Remove(currentCart);
                }


                //_context.Orders.Add(order);
            }
            return _context.SaveChanges();
        }

        public int DelOrder(int OrderID)
        {
            return 0;
        }


    }
}
