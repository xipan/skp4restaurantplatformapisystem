﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFCartRepository : ICartRepository                 
    {
        private EFDbContext _context;

        public EFCartRepository(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Cart> Carts
        {
            get
            {
                return _context.Carts.Include(c=>c.CartItem);
            }
        }

        public Cart SaveCart(Cart cart)
        {
            if (cart.CartID == 0)
            {
                List<Cart> isExistence = _context.Carts.Where(c => (c.RestaurantID == cart.RestaurantID) && (c.UserID == cart.UserID)).ToList();
                if (isExistence.Count == 0)
                {
                    _context.Carts.Add(cart);
                }                
            }
            else
            {
                Cart dbEntry = _context.Carts.
                    Where(c => (c.RestaurantID == cart.RestaurantID) && (c.UserID == cart.UserID)).
                    Include(c => c.CartItem).
                    SingleOrDefault();
                if (dbEntry != null)
                {
                    foreach (var item in dbEntry.CartItem)
                    {
                        _context.CartItems.Remove(item);
                    }

                    foreach (var item in cart.CartItem)
                    {
                        item.CartID = cart.CartID;
                        _context.CartItems.Add(item);
                    }
                }
            }

            if (_context.SaveChanges() >= 1)
            {
                return cart;
            }
            else
            {
                return null;
            }
        }

        public int DelCart(int cartId)
        {
            if (cartId <= 0)
            {
                return 0;
            }
            else
            {
                //User dbEntry = _context.Users.Find(userId);
                //if (dbEntry != null)
                //{
                //    _context.Remove(dbEntry);
                //}
            }
            return _context.SaveChanges();
        }


    }
}
