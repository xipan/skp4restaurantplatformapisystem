﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using RestaurantPlatformApiSystem.Domain.Abstract;
using RestaurantPlatformApiSystem.Domain.Entities;

namespace RestaurantPlatformApiSystem.Domain.Concrete
{
    public class EFRestaurantRepository : IRestaurantRepository
    {
        private EFDbContext _context;

        public EFRestaurantRepository(EFDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Restaurant> Restaurants
        {
            get
            {
                return _context.Restaurants.Include(r => r.Table);
                //.Include(r => r.Table)
            }
        }

        public void SaveRestaurant(Restaurant restaurant)
        {
            if (restaurant.RestaurantID == 0)
            {
                _context.Restaurants.Add(restaurant);
            }
            else
            {
                Restaurant dbEntry = _context.Restaurants.Find(restaurant.RestaurantID);
                if (dbEntry != null)
                {
                    dbEntry.RestaturantName = restaurant.RestaturantName;
                    dbEntry.Adresse = restaurant.Adresse;
                    dbEntry.Postale = restaurant.Postale;
                    dbEntry.OpenStart = restaurant.OpenStart;
                    dbEntry.OpenEnd = restaurant.OpenEnd;
                    dbEntry.RestaurantLogo = restaurant.RestaurantLogo;
                }
            }
            _context.SaveChanges();

        }

    }
}
