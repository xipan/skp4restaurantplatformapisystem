﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class RenameMenuCategories : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuCategory_Restaurants_RestaurantID",
                table: "MenuCategory");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuItem_MenuCategory_MenuCategoryID",
                table: "MenuItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MenuItem",
                table: "MenuItem");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MenuCategory",
                table: "MenuCategory");

            migrationBuilder.RenameTable(
                name: "MenuItem",
                newName: "MenuItems");

            migrationBuilder.RenameTable(
                name: "MenuCategory",
                newName: "MenuCategories");

            migrationBuilder.RenameIndex(
                name: "IX_MenuItem_MenuCategoryID",
                table: "MenuItems",
                newName: "IX_MenuItems_MenuCategoryID");

            migrationBuilder.RenameIndex(
                name: "IX_MenuCategory_RestaurantID",
                table: "MenuCategories",
                newName: "IX_MenuCategories_RestaurantID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MenuItems",
                table: "MenuItems",
                column: "MenuItemID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MenuCategories",
                table: "MenuCategories",
                column: "MenuCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuCategories_Restaurants_RestaurantID",
                table: "MenuCategories",
                column: "RestaurantID",
                principalTable: "Restaurants",
                principalColumn: "RestaurantID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItems_MenuCategories_MenuCategoryID",
                table: "MenuItems",
                column: "MenuCategoryID",
                principalTable: "MenuCategories",
                principalColumn: "MenuCategoryID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MenuCategories_Restaurants_RestaurantID",
                table: "MenuCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_MenuItems_MenuCategories_MenuCategoryID",
                table: "MenuItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MenuItems",
                table: "MenuItems");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MenuCategories",
                table: "MenuCategories");

            migrationBuilder.RenameTable(
                name: "MenuItems",
                newName: "MenuItem");

            migrationBuilder.RenameTable(
                name: "MenuCategories",
                newName: "MenuCategory");

            migrationBuilder.RenameIndex(
                name: "IX_MenuItems_MenuCategoryID",
                table: "MenuItem",
                newName: "IX_MenuItem_MenuCategoryID");

            migrationBuilder.RenameIndex(
                name: "IX_MenuCategories_RestaurantID",
                table: "MenuCategory",
                newName: "IX_MenuCategory_RestaurantID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MenuItem",
                table: "MenuItem",
                column: "MenuItemID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_MenuCategory",
                table: "MenuCategory",
                column: "MenuCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_MenuCategory_Restaurants_RestaurantID",
                table: "MenuCategory",
                column: "RestaurantID",
                principalTable: "Restaurants",
                principalColumn: "RestaurantID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_MenuItem_MenuCategory_MenuCategoryID",
                table: "MenuItem",
                column: "MenuCategoryID",
                principalTable: "MenuCategory",
                principalColumn: "MenuCategoryID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
