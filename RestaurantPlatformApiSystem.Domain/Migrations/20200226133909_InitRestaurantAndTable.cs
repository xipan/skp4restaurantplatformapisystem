﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class InitRestaurantAndTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Restaurants",
                columns: new[] { "RestaurantID", "Adresse", "OpenEnd", "OpenStart", "Postale", "RestaturantName", "RestaurantLogo" },
                values: new object[,]
                {
                    { 1, "Vangede Bygade 88, Gentofte,2820", new TimeSpan(0, 14, 30, 0, 0), new TimeSpan(0, 22, 30, 0, 0), "2820", "Mezzaluna", "18283.gif" },
                    { 2, "Vangedevej 2, Gentofte,2820", new TimeSpan(0, 14, 30, 0, 0), new TimeSpan(0, 22, 30, 0, 0), "2820", "Pizzamed", "15120.gif" },
                    { 3, "Baunegårdsvej 7, Gentofte, 2820", new TimeSpan(0, 14, 30, 0, 0), new TimeSpan(0, 21, 30, 0, 0), "2820", "Big Bens Pizza & Pasta", "11729.gif" },
                    { 4, "Lyngby Torv 7, Kongens Lyngby, 2800", new TimeSpan(0, 14, 30, 0, 0), new TimeSpan(0, 22, 30, 0, 0), "2820", "NamNam Slik - Lyngby", "18295.gif" }
                });

            migrationBuilder.InsertData(
                table: "Tables",
                columns: new[] { "TableID", "RestaurantID", "TableLabel", "TableStatus" },
                values: new object[,]
                {
                    { 1, 1, "1", 0 },
                    { 2, 1, "2", 0 },
                    { 3, 1, "3", 0 },
                    { 4, 2, "1", 0 },
                    { 5, 2, "2", 0 },
                    { 6, 2, "3", 0 },
                    { 7, 3, "1", 0 },
                    { 8, 3, "2", 0 },
                    { 9, 3, "3", 0 },
                    { 10, 4, "1", 0 },
                    { 11, 4, "2", 0 },
                    { 12, 4, "3", 0 },
                    { 13, 4, "4", 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Tables",
                keyColumn: "TableID",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Restaurants",
                keyColumn: "RestaurantID",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Restaurants",
                keyColumn: "RestaurantID",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Restaurants",
                keyColumn: "RestaurantID",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Restaurants",
                keyColumn: "RestaurantID",
                keyValue: 4);
        }
    }
}
