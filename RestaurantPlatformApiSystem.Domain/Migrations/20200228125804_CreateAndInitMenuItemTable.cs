﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class CreateAndInitMenuItemTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MenuItem",
                columns: table => new
                {
                    MenuItemID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    MenuItemName = table.Column<string>(nullable: true),
                    Price = table.Column<float>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    MenuCategoryID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuItem", x => x.MenuItemID);
                    table.ForeignKey(
                        name: "FK_MenuItem_MenuCategory_MenuCategoryID",
                        column: x => x.MenuCategoryID,
                        principalTable: "MenuCategory",
                        principalColumn: "MenuCategoryID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "MenuItem",
                columns: new[] { "MenuItemID", "Description", "MenuCategoryID", "MenuItemName", "Price" },
                values: new object[,]
                {
                    { 1, null, 1, "Coca-Cola(0.5L)", 20f },
                    { 24, null, 15, "M&M's(Peanut Butter 46g)", 20f },
                    { 23, null, 14, "Fanta Exotic 0,33 L", 12f },
                    { 22, null, 14, "Fanta 0,33 L", 12f },
                    { 21, null, 14, "Gazoz 0, 33 L", 12f },
                    { 20, null, 14, "Capri-Sonne 330 ml.(Orange & Peach)", 14f },
                    { 19, null, 14, "Capri-Sonne 330 ml.(Med brus)", 14f },
                    { 18, null, 14, "Aqua Vand 0,50 L(Med brus)", 14f },
                    { 17, null, 3, "Kyllingespyd (2 Stk.) med 3 Slags Salater", 80f },
                    { 16, "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", 3, "Lille Bakke", 25f },
                    { 15, "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", 3, "Mellem Bakke", 40f },
                    { 14, "Rødedesalat / Tzatziki / Hummus / Bulgur / Græsk salat / Spinatsalat / Kikærtesalat / Kartoffelsalat / Artiskoksalat / Broccolisalat / Tunsalat", 3, "Lille Bakke", 25f },
                    { 13, "Hjemmelavet pizza sandwich, mellem pommes frites, salatmayonnaise og 0,5L sodavand", 2, "Menu 5", 89f },
                    { 12, "Hjemmelavet durum rulle, mellem pommes frites, salatmayonnaise og 0,5L sodavand", 2, "Menu 4", 89f },
                    { 11, "Hjemmelavet husets sandwich, mellem pommes frites, salatmayonnaise og dåse sodavand", 2, "Menu 3", 79f },
                    { 10, "Hjemmelavet pitabrød, mellem pommes frites, salatmayonnaise og dåse sodavand", 2, "Menu 2", 69f },
                    { 9, "Hjemmelavet big burger, mellem pommes frites, salatmayonnaise og dåse sodavand", 2, "Menu 1", 69f },
                    { 8, null, 1, "Husets Hvidvin", 69f },
                    { 7, null, 1, "Nestea", 20f },
                    { 6, null, 1, "Cocio", 20f },
                    { 5, null, 1, "Kildevand(0.5L)", 10f },
                    { 4, null, 1, "Faxe Kondi(0.5L)", 12f },
                    { 3, null, 1, "Tuborg(Dåse)", 12f },
                    { 2, null, 1, "Coca-Cola Zero(0.5L)", 20f },
                    { 25, null, 15, "M&M's(White Chocolate 46g)", 20f },
                    { 26, null, 15, "M&M's(Peanut Butter 85g)", 34f }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuItem_MenuCategoryID",
                table: "MenuItem",
                column: "MenuCategoryID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MenuItem");
        }
    }
}
