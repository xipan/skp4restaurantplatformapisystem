﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using RestaurantPlatformApiSystem.Domain.Concrete;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    [DbContext(typeof(EFDbContext))]
    [Migration("20200228114804_CreateMenuCategoryTable")]
    partial class CreateMenuCategoryTable
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.1")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.MenuCategory", b =>
                {
                    b.Property<int>("MenuCategoryID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("CategoryName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("RestaurantID")
                        .HasColumnType("int");

                    b.HasKey("MenuCategoryID");

                    b.HasIndex("RestaurantID");

                    b.ToTable("MenuCategory");

                    b.HasData(
                        new
                        {
                            MenuCategoryID = 1,
                            CategoryName = "Drikke",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 2,
                            CategoryName = "Menuer",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 3,
                            CategoryName = "Salater",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 4,
                            CategoryName = "Husets sandwich",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 5,
                            CategoryName = "Pizzasandwich",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 6,
                            CategoryName = "Pizza'er",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 7,
                            CategoryName = "Børne Pizza",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 8,
                            CategoryName = "Pastaretter",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 9,
                            CategoryName = "A la carte",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 10,
                            CategoryName = "Hjemmelavet Burger",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 11,
                            CategoryName = "Hjemmelavet Pitabrød",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 12,
                            CategoryName = "Diverse",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 13,
                            CategoryName = "Bagt kartoffel",
                            RestaurantID = 1
                        },
                        new
                        {
                            MenuCategoryID = 14,
                            CategoryName = "Drikkevarer",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 15,
                            CategoryName = "Amerikansk & special slik",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 16,
                            CategoryName = "Chokolade",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 17,
                            CategoryName = "Chokoladeplader",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 18,
                            CategoryName = "Amerikanske og specielle drikkevarer",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 19,
                            CategoryName = "Nødder",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 20,
                            CategoryName = "Sjov slik",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 21,
                            CategoryName = "Slikposer",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 22,
                            CategoryName = "Snacks",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 23,
                            CategoryName = "Gum & Pastilles",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 24,
                            CategoryName = "Is",
                            RestaurantID = 4
                        },
                        new
                        {
                            MenuCategoryID = 25,
                            CategoryName = "Mix Menu",
                            RestaurantID = 4
                        });
                });

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.Restaurant", b =>
                {
                    b.Property<int>("RestaurantID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Adresse")
                        .HasColumnType("nvarchar(max)");

                    b.Property<TimeSpan>("OpenEnd")
                        .HasColumnType("time");

                    b.Property<TimeSpan>("OpenStart")
                        .HasColumnType("time");

                    b.Property<string>("Postale")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RestaturantName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RestaurantLogo")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("RestaurantID");

                    b.ToTable("Restaurants");

                    b.HasData(
                        new
                        {
                            RestaurantID = 1,
                            Adresse = "Vangede Bygade 88, Gentofte,2820",
                            OpenEnd = new TimeSpan(0, 14, 30, 0, 0),
                            OpenStart = new TimeSpan(0, 22, 30, 0, 0),
                            Postale = "2820",
                            RestaturantName = "Mezzaluna",
                            RestaurantLogo = "18283.gif"
                        },
                        new
                        {
                            RestaurantID = 2,
                            Adresse = "Vangedevej 2, Gentofte,2820",
                            OpenEnd = new TimeSpan(0, 14, 30, 0, 0),
                            OpenStart = new TimeSpan(0, 22, 30, 0, 0),
                            Postale = "2820",
                            RestaturantName = "Pizzamed",
                            RestaurantLogo = "15120.gif"
                        },
                        new
                        {
                            RestaurantID = 3,
                            Adresse = "Baunegårdsvej 7, Gentofte, 2820",
                            OpenEnd = new TimeSpan(0, 14, 30, 0, 0),
                            OpenStart = new TimeSpan(0, 21, 30, 0, 0),
                            Postale = "2820",
                            RestaturantName = "Big Bens Pizza & Pasta",
                            RestaurantLogo = "11729.gif"
                        },
                        new
                        {
                            RestaurantID = 4,
                            Adresse = "Lyngby Torv 7, Kongens Lyngby, 2800",
                            OpenEnd = new TimeSpan(0, 14, 30, 0, 0),
                            OpenStart = new TimeSpan(0, 22, 30, 0, 0),
                            Postale = "2820",
                            RestaturantName = "NamNam Slik - Lyngby",
                            RestaurantLogo = "18295.gif"
                        });
                });

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.Table", b =>
                {
                    b.Property<int>("TableID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("RestaurantID")
                        .HasColumnType("int");

                    b.Property<string>("TableLabel")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("TableStatus")
                        .HasColumnType("int");

                    b.HasKey("TableID");

                    b.HasIndex("RestaurantID");

                    b.ToTable("Tables");

                    b.HasData(
                        new
                        {
                            TableID = 1,
                            RestaurantID = 1,
                            TableLabel = "1",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 2,
                            RestaurantID = 1,
                            TableLabel = "2",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 3,
                            RestaurantID = 1,
                            TableLabel = "3",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 4,
                            RestaurantID = 2,
                            TableLabel = "1",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 5,
                            RestaurantID = 2,
                            TableLabel = "2",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 6,
                            RestaurantID = 2,
                            TableLabel = "3",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 7,
                            RestaurantID = 3,
                            TableLabel = "1",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 8,
                            RestaurantID = 3,
                            TableLabel = "2",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 9,
                            RestaurantID = 3,
                            TableLabel = "3",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 10,
                            RestaurantID = 4,
                            TableLabel = "1",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 11,
                            RestaurantID = 4,
                            TableLabel = "2",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 12,
                            RestaurantID = 4,
                            TableLabel = "3",
                            TableStatus = 0
                        },
                        new
                        {
                            TableID = 13,
                            RestaurantID = 4,
                            TableLabel = "4",
                            TableStatus = 0
                        });
                });

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.User", b =>
                {
                    b.Property<int>("UserID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("AfterName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("FirstName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PassWord")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("UserType")
                        .HasColumnType("int");

                    b.HasKey("UserID");

                    b.ToTable("Users");

                    b.HasData(
                        new
                        {
                            UserID = 1,
                            AfterName = "Pan",
                            Email = "xipanpost@gmail.com",
                            FirstName = "Xi",
                            PassWord = "password",
                            UserType = 0
                        },
                        new
                        {
                            UserID = 2,
                            AfterName = "Jacobsen",
                            Email = "frederik@gmail.com",
                            FirstName = "Frederik",
                            PassWord = "password",
                            UserType = 0
                        },
                        new
                        {
                            UserID = 3,
                            AfterName = "Nielsen",
                            Email = "aksel@gmail.com",
                            FirstName = "Aksel",
                            PassWord = "password",
                            UserType = 0
                        },
                        new
                        {
                            UserID = 4,
                            AfterName = "Berman ",
                            Email = "julie@gmail.com",
                            FirstName = "Julie",
                            PassWord = "password",
                            UserType = 0
                        });
                });

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.MenuCategory", b =>
                {
                    b.HasOne("RestaurantPlatformApiSystem.Domain.Entities.Restaurant", "Restaurant")
                        .WithMany("MenuCategory")
                        .HasForeignKey("RestaurantID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("RestaurantPlatformApiSystem.Domain.Entities.Table", b =>
                {
                    b.HasOne("RestaurantPlatformApiSystem.Domain.Entities.Restaurant", "Restaurant")
                        .WithMany("Table")
                        .HasForeignKey("RestaurantID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
