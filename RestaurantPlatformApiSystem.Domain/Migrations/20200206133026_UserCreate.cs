﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class UserCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    UserID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    AfterName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    PassWord = table.Column<string>(nullable: true),
                    UserType = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.UserID);
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "UserID", "AfterName", "Email", "FirstName", "PassWord", "UserType" },
                values: new object[,]
                {
                    { 1, "Pan", "xipanpost@gmail.com", "Xi", "password", 0 },
                    { 2, "Jacobsen", "frederik@gmail.com", "Frederik", "password", 0 },
                    { 3, "Nielsen", "aksel@gmail.com", "Aksel", "password", 0 },
                    { 4, "Berman ", "julie@gmail.com", "Julie", "password", 0 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
