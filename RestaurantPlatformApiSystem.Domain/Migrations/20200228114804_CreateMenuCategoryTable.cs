﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class CreateMenuCategoryTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "MenuCategory",
                columns: table => new
                {
                    MenuCategoryID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CategoryName = table.Column<string>(nullable: true),
                    RestaurantID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MenuCategory", x => x.MenuCategoryID);
                    table.ForeignKey(
                        name: "FK_MenuCategory_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "RestaurantID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "MenuCategory",
                columns: new[] { "MenuCategoryID", "CategoryName", "RestaurantID" },
                values: new object[,]
                {
                    { 1, "Drikke", 1 },
                    { 23, "Gum & Pastilles", 4 },
                    { 22, "Snacks", 4 },
                    { 21, "Slikposer", 4 },
                    { 20, "Sjov slik", 4 },
                    { 19, "Nødder", 4 },
                    { 18, "Amerikanske og specielle drikkevarer", 4 },
                    { 17, "Chokoladeplader", 4 },
                    { 16, "Chokolade", 4 },
                    { 15, "Amerikansk & special slik", 4 },
                    { 14, "Drikkevarer", 4 },
                    { 24, "Is", 4 },
                    { 13, "Bagt kartoffel", 1 },
                    { 11, "Hjemmelavet Pitabrød", 1 },
                    { 10, "Hjemmelavet Burger", 1 },
                    { 9, "A la carte", 1 },
                    { 8, "Pastaretter", 1 },
                    { 7, "Børne Pizza", 1 },
                    { 6, "Pizza'er", 1 },
                    { 5, "Pizzasandwich", 1 },
                    { 4, "Husets sandwich", 1 },
                    { 3, "Salater", 1 },
                    { 2, "Menuer", 1 },
                    { 12, "Diverse", 1 },
                    { 25, "Mix Menu", 4 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_MenuCategory_RestaurantID",
                table: "MenuCategory",
                column: "RestaurantID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MenuCategory");
        }
    }
}
