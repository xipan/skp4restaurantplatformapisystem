﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace RestaurantPlatformApiSystem.Domain.Migrations
{
    public partial class CreateTableAndRestaurant : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Adresse",
                table: "Restaurants",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OpenEnd",
                table: "Restaurants",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "OpenStart",
                table: "Restaurants",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Postale",
                table: "Restaurants",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "RestaturantName",
                table: "Restaurants",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Tables",
                columns: table => new
                {
                    TableID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TableLabel = table.Column<string>(nullable: true),
                    TableStatus = table.Column<int>(nullable: false),
                    RestaurantID = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tables", x => x.TableID);
                    table.ForeignKey(
                        name: "FK_Tables_Restaurants_RestaurantID",
                        column: x => x.RestaurantID,
                        principalTable: "Restaurants",
                        principalColumn: "RestaurantID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Tables_RestaurantID",
                table: "Tables",
                column: "RestaurantID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tables");

            migrationBuilder.DropColumn(
                name: "Adresse",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "OpenEnd",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "OpenStart",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "Postale",
                table: "Restaurants");

            migrationBuilder.DropColumn(
                name: "RestaturantName",
                table: "Restaurants");
        }
    }
}
