﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformApiSystem.Domain.Entities
{
    public class Cart
    {
        public int CartID { get; set; }
        public int RestaurantID { get; set; }
        public int UserID { get; set; }
        public DateTime CreateDT { get; set; }

        public ICollection<CartItem> CartItem { get; set; }

    }

    public class CartItem
    {
        public int CartItemID { get; set; }
        public string MenuItemName { get; set; }
        public float Price { get; set; }
        public int Quantity { get; set; }
        public int Status { get; set; }
        public int RefMenuItemID { get; set; }


        public int CartID { get; set; }
        public Cart Cart { get; set; }

    }
}
