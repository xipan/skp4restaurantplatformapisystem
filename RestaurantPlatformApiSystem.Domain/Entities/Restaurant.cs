﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformApiSystem.Domain.Entities
{
    public class Restaurant
    {
        public int RestaurantID { get; set; }
        public string RestaturantName { get; set; }
        public string Adresse { get; set; }
        public string Postale { get; set; }
        public TimeSpan OpenStart { get; set; }
        public TimeSpan OpenEnd { get; set; }
        public string RestaurantLogo { get; set; }

        public ICollection<Table> Table { get; set; }
        public ICollection<MenuCategory> MenuCategory { get; set; }
        public ICollection<Order> Order { get; set; }

    }
}
