﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformApiSystem.Domain.Entities
{
    public class MenuItem
    {
        public int MenuItemID { get; set; }
        public string MenuItemName { get; set; }
        public float Price { get; set; }
        public string Description { get; set; }

        public int MenuCategoryID { get; set; }
        public MenuCategory MenuCategory { get; set; }
    }
}
