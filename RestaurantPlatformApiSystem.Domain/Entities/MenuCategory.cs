﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestaurantPlatformApiSystem.Domain.Entities
{
    public class MenuCategory
    {
        public int MenuCategoryID { get; set; }
        public string CategoryName { get; set; }        

        public int RestaurantID { get; set; }
        public Restaurant Restaurant { get; set; }

        public ICollection<MenuItem> MenuItem { get; set; }

    }
}
